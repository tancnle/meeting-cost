import { h } from 'preact';

import MeetingStatus from '../MeetingStatus';
import { mockMeeting } from '../../use-meeting';

jest.mock('../../use-meeting');

describe('MeetingStatus', () => {
  it('renders', () => {
    jest.spyOn(Date, 'now').mockReturnValue(123456789);
    mockMeeting({
      id: '12345',
      name: 'Example Meeting',
      rate: 0.1234,
      meetingStart: Date.now() - 1234000,
      checkpoint: {
        value: 0,
        time: Date.now() - 2345000
      }
    });

    const wrapper = shallow(
      <MeetingStatus />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders nothing when there is no meeting', () => {
    mockMeeting({});

    const wrapper = shallow(
      <MeetingStatus />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
