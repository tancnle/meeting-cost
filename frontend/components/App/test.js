import { h } from 'preact';

import App from '../App';
import useLocation from '../../use-location';
import { mockMeeting, attend } from '../../use-meeting';

jest.mock('../../use-meeting');
jest.mock('../../use-location');

describe('App', () => {
  beforeEach(() => {
    useLocation.mockReturnValue({
      location: { pathname: '/' }
    });
  });

  it('renders', () => {
    const wrapper = shallow(
      <App />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('includes the meeting name in the title', () => {
    mockMeeting({
      name: 'Example Meeting'
    });

    const wrapper = shallow(
      <App />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('watches the meeting from the URL if not already watching', () => {
    useLocation.mockReturnValue({
      location: { pathname: '/12345' }
    });

    shallow(
      <App />
    );

    expect(attend).toHaveBeenCalledWith({ id: '12345', rate: 0 });
  });

  it('logs an error when watching fails', async () => {
    jest.spyOn(console, 'error').mockImplementation(() => {});
    attend.mockRejectedValue('example error');
    useLocation.mockReturnValue({
      location: { pathname: '/12345' }
    });

    shallow(
      <App />
    );

    await flushPromises();

    expect(console.error).toHaveBeenCalled();
  });

  it('does not watch the meeting if already watching it', () => {
    mockMeeting({
      id: '12345'
    });
    useLocation.mockReturnValue({
      location: { pathname: '/12345' }
    });

    shallow(
      <App />
    );

    expect(attend).not.toHaveBeenCalled();
  });
});
