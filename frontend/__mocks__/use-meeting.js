const mockedMeeting = jest.fn();
const start = jest.fn();
const attend = jest.fn();
const rename = jest.fn();

const mockMeeting = meeting => mockedMeeting.mockReturnValue(meeting);

const useMeeting = () => ({
  ...mockedMeeting(),
  start: async (...args) => start(...args),
  attend: async (...args) => attend(...args),
  rename: async (...args) => rename(...args)
});

beforeEach(() => {
  mockedMeeting.mockReset();
  start.mockReset();
  attend.mockReset();
  rename.mockReset();

  mockMeeting({});
});

export default useMeeting;
export {
  mockMeeting,
  start,
  attend,
  rename
};
